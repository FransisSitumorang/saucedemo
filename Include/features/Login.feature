@Login @Smoke
Feature: Login
  As a user, I want to login in Swag Labs Web

  @LGI001
  Scenario: LGI001
    Then User click on LOGIN button no credential

  @LGI002
  Scenario: LGI002
    Then User input registered username only standard_user
    Then User click on LOGIN button only username

  @LGI003
  Scenario: LGI003
    Then User input registered username standard_user
    Then User input registered password secret_sauce
    Then User click on LOGIN button valid credential
