<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login_logo</name>
   <tag></tag>
   <elementGuidId>42c06231-e220-49bd-99fd-59912e802cae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.login_logo</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>56978ebf-cd14-4b00-b1ce-dcb153208169</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login_logo</value>
      <webElementGuid>013cfbc9-616e-4365-a541-b0b8a5075920</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;login_logo&quot;]</value>
      <webElementGuid>014b5bd5-234c-412a-b980-51b1d4d26a37</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div</value>
      <webElementGuid>354c234e-a526-40c5-a58e-ac697a1c3ed8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
      <webElementGuid>ccc3e32c-4e72-4fae-8d69-6376d456f394</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
